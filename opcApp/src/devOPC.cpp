/* $Header: $ */

/* Patrick J. Thomas, California Institute of Technology, LIGO Hanford */

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>

#include <string>
#include <set>
#include <map>

#include "alarm.h"
#include "cvtTable.h"
#include "dbDefs.h"
#include "dbAccess.h"
#include "initHooks.h"
#include "recGbl.h"
#include "recSup.h"
#include "devSup.h"
#include "link.h"
#include "biRecord.h"
#include "longinRecord.h"
#include "aiRecord.h"
#include "stringinRecord.h"
#include "boRecord.h"
#include "longoutRecord.h"
#include "aoRecord.h"
#include "stringoutRecord.h"
#include "epicsExport.h"

#include "iocsh.h"

#include "open62541.h"


int get_value(std::string node_id_xml, UA_Variant* p_value);
void* read_loop(void* arg);


UA_Client *client;
std::string url;

std::set<std::string> node_id_xml_set;
std::map<std::string, int> node_id_xml_to_index_map;

UA_ReadResponse read_response;

pthread_t thread;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;


/* bi */

static long init_bi_record(struct biRecord* prec);
static long read_bi(struct biRecord* prec);
struct bi_DevSet_type {
	long number;
	DEVSUPFUN report;
	DEVSUPFUN init;
	long (*init_bi_record)(struct biRecord*);
	DEVSUPFUN	get_ioint_info;
	long (*read_bi)(struct biRecord*);
	DEVSUPFUN	special_linconv;
} bi_DevSet = {
	6,
	NULL,
	NULL,
	init_bi_record,
	NULL,
	read_bi,
	NULL
};
epicsExportAddress(dset, bi_DevSet);

static long init_bi_record(struct biRecord* prec) {
	std::string node_id_xml;


	if (prec->inp.type != INST_IO) {
		recGblRecordError(S_db_badField, (void*) prec, "bi_DevSet (init_bi_record) Illegal INP field");

		return S_db_badField;
	}

	node_id_xml = std::string(prec->inp.value.instio.string);

	/* Stores the fully qualified NodeId in XML notation defined by the OPC UA XML Schema. */
	/* ns=<namespaceIndex>;<identifiertype>=<identifier> */
	node_id_xml_set.insert(node_id_xml);

	return 0;
}

/*
0: Success. A new raw value is placed in RVAL. The record support module forces VAL to be (0,1) if RVAL is (0, not 0).
2: Success, but don't modify VAL.
other: Error.
*/
static long read_bi(struct biRecord *prec) {
	std::string node_id_xml;
	UA_Variant value;


	if (prec->inp.type != INST_IO) {
		recGblRecordError(S_db_badField, (void*) prec, "bi_DevSet (read_bi) Illegal INP field");

		return S_db_badField;
	}

	node_id_xml = std::string(prec->inp.value.instio.string);
	UA_Variant_init(&value);

	if (get_value(node_id_xml, &value)) {
		recGblSetSevr(prec, READ_ALARM, INVALID_ALARM);

		return -1;
	}

	if (!UA_Variant_hasScalarType(&value, &UA_TYPES[UA_TYPES_BOOLEAN])) {
		recGblSetSevr(prec, READ_ALARM, INVALID_ALARM);

		return -1;
	}

	prec->rval = (epicsUInt32) (*(UA_Boolean*) value.data);

	return 0;
}


/* longin */

static long init_longin_record(struct longinRecord* prec);
static long read_longin(struct longinRecord *prec);
struct longin_DevSet_type {
	long number;
	DEVSUPFUN report;
	DEVSUPFUN init;
	long (*init_longin_record)(struct longinRecord*);
	DEVSUPFUN	get_ioint_info;
	long (*read_longin)(struct longinRecord*);
	DEVSUPFUN	special_linconv;
} longin_DevSet = {
	6,
	NULL,
	NULL,
	init_longin_record,
	NULL,
	read_longin,
	NULL
};
epicsExportAddress(dset, longin_DevSet);

static long init_longin_record(struct longinRecord* prec) {
	std::string node_id_xml;


	if (prec->inp.type != INST_IO) {
		recGblRecordError(S_db_badField, (void*) prec, "longin_DevSet (init_longin_record) Illegal INP field");

		return S_db_badField;
	}

	node_id_xml = std::string(prec->inp.value.instio.string);

	/* Stores the fully qualified NodeId in XML notation defined by the OPC UA XML Schema. */
	/* ns=<namespaceIndex>;<identifiertype>=<identifier> */
	node_id_xml_set.insert(node_id_xml);

	return 0;
}

/*
0: Success. A new value is placed in VAL.
Other: Error.
*/
static long read_longin(struct longinRecord *prec) {
	std::string node_id_xml;
	UA_Variant value;


	if (prec->inp.type != INST_IO) {
		recGblRecordError(S_db_badField, (void*) prec, "longin_DevSet (read_longin) Illegal INP field");

		return S_db_badField;
	}

	node_id_xml = std::string(prec->inp.value.instio.string);
	UA_Variant_init(&value);

	if (get_value(node_id_xml, &value)) {
		recGblSetSevr(prec, READ_ALARM, INVALID_ALARM);

		return -1;
	}

	if (!UA_Variant_hasScalarType(&value, &UA_TYPES[UA_TYPES_INT16])) {
		recGblSetSevr(prec, READ_ALARM, INVALID_ALARM);

		return -1;
	}

	prec->val = (epicsInt32) (*(UA_Int16*) value.data);

	return 0;
}


/* ai */

static long init_ai_record(struct aiRecord* prec);
static long read_ai(struct aiRecord* prec);
struct ai_DevSet_type {
	long number;
	DEVSUPFUN report;
	DEVSUPFUN init;
	long (*init_ai_record)(struct aiRecord*);
	DEVSUPFUN	get_ioint_info;
	long (*read_ai)(struct aiRecord*);
	DEVSUPFUN	special_linconv;
} ai_DevSet = {
	6,
	NULL,
	NULL,
	init_ai_record,
	NULL,
	read_ai,
	NULL
};
epicsExportAddress(dset, ai_DevSet);

static long init_ai_record(struct aiRecord* prec) {
	std::string node_id_xml;


	if (prec->inp.type != INST_IO) {
		recGblRecordError(S_db_badField, (void*) prec, "ai_DevSet (init_ai_record) Illegal INP field");

		return S_db_badField;
	}

	node_id_xml = std::string(prec->inp.value.instio.string);

	/* Stores the fully qualified NodeId in XML notation defined by the OPC UA XML Schema. */
	/* ns=<namespaceIndex>;<identifiertype>=<identifier> */
	node_id_xml_set.insert(node_id_xml);

	return 0;
}

/*
0: Success. A new raw value is placed in RVAL. convert will be called.
2: Success, but don't call convert. This is useful if read_ai obtains a value already converted to engineering units or in the event a hardware failure is detected.
Other: Error.
*/
static long read_ai(struct aiRecord *prec) {
	std::string node_id_xml;
	UA_Variant value;


	if (prec->inp.type != INST_IO) {
		recGblRecordError(S_db_badField, (void*) prec, "ai_DevSet (read_ai) Illegal INP field");

		return S_db_badField;
	}

	node_id_xml = std::string(prec->inp.value.instio.string);
	UA_Variant_init(&value);

	if (get_value(node_id_xml, &value)) {
		recGblSetSevr(prec, READ_ALARM, INVALID_ALARM);

		return -1;
	}

	if (!UA_Variant_hasScalarType(&value, &UA_TYPES[UA_TYPES_DOUBLE])) {
		recGblSetSevr(prec, READ_ALARM, INVALID_ALARM);

		return -1;
	}

	prec->val = (epicsFloat64) (*(UA_Double*) value.data);
	prec->udf = 0;

	return 2;
}


/* stringin */

static long init_stringin_record(struct stringinRecord* prec);
static long read_stringin(struct stringinRecord *prec);
struct stringin_DevSet_type {
	long number;
	DEVSUPFUN report;
	DEVSUPFUN init;
	long (*init_stringin_record)(struct stringinRecord*);
	DEVSUPFUN	get_ioint_info;
	long (*read_stringin)(struct stringinRecord*);
	DEVSUPFUN	special_linconv;
} stringin_DevSet = {
	6,
	NULL,
	NULL,
	init_stringin_record,
	NULL,
	read_stringin,
	NULL
};
epicsExportAddress(dset, stringin_DevSet);

static long init_stringin_record(struct stringinRecord* prec) {
	std::string node_id_xml;


	if (prec->inp.type != INST_IO) {
		recGblRecordError(S_db_badField, (void*) prec, "stringin_DevSet (init_stringin_record) Illegal INP field");

		return S_db_badField;
	}

	node_id_xml = std::string(prec->inp.value.instio.string);

	/* Stores the fully qualified NodeId in XML notation defined by the OPC UA XML Schema. */
	/* ns=<namespaceIndex>;<identifiertype>=<identifier> */
	node_id_xml_set.insert(node_id_xml);

	return 0;
}

/*
0: Success. A new ASCII string is stored into VAL.
Other: Error.
*/
static long read_stringin(struct stringinRecord *prec) {
	std::string node_id_xml;
	UA_Variant value;
	UA_String s;
	size_t val_len;


	if (prec->inp.type != INST_IO) {
		recGblRecordError(S_db_badField, (void*) prec, "stringin_DevSet (read_stringin) Illegal INP field");

		return S_db_badField;
	}

	node_id_xml = std::string(prec->inp.value.instio.string);
	UA_Variant_init(&value);

	if (get_value(node_id_xml, &value)) {
		recGblSetSevr(prec, READ_ALARM, INVALID_ALARM);

		return -1;
	}

	if (!UA_Variant_hasScalarType(&value, &UA_TYPES[UA_TYPES_STRING])) {
		recGblSetSevr(prec, READ_ALARM, INVALID_ALARM);

		return -1;
	}

	val_len = sizeof(prec->val) / sizeof(prec->val[0]);

	s = *(UA_String*) value.data;

	if (s.length < val_len) {
		strncpy(prec->val, (char*) s.data, s.length);
		prec->val[s.length] = '\0';
	}
	else {
		strncpy(prec->val, (char*) s.data, val_len - 1);
		prec->val[val_len - 1] = '\0';
	}

	return 0;
}


/* bo */

static long init_bo_record(struct boRecord *prec);
static long write_bo(struct boRecord *prec);
struct bo_DevSet_type {
	long number;
	DEVSUPFUN report;
	DEVSUPFUN init;
	long (*init_bo_record)(struct boRecord*);
	DEVSUPFUN get_ioint_info;
	long (*write_bo)(struct boRecord*);
	DEVSUPFUN special_linconv;
} bo_DevSet = {
	6,
	NULL,
	NULL,
	init_bo_record,
	NULL,
	write_bo,
	NULL
};
epicsExportAddress(dset, bo_DevSet);

static long init_bo_record(struct boRecord *prec) {
	if (prec->out.type != INST_IO) {
		recGblRecordError(S_db_badField, (void*) prec, "bo_DevSet (init_bo_record) Illegal OUT field");

		return S_db_badField;
	}

	return 2;
}

/*
0: Success.
other: Error.
*/
static long write_bo(struct boRecord *prec) {
	int ns;
	std::string node_id_xml;
	UA_Variant val;
	UA_StatusCode retval;


	if (prec->out.type != INST_IO) {
		recGblRecordError(S_db_badField, (void*) prec, "bo_DevSet (write_bo) Illegal OUT field");

		return S_db_badField;
	}

	ns = 4;
	node_id_xml = std::string(prec->out.value.instio.string);

	UA_Variant_init(&val);
	retval = UA_Variant_setScalarCopy(&val, &(prec->val), &UA_TYPES[UA_TYPES_BOOLEAN]);
	if (retval != UA_STATUSCODE_GOOD) {
		recGblSetSevr(prec, WRITE_ALARM, INVALID_ALARM);

		return -1;
	}

	pthread_mutex_lock(&mutex);
	retval = UA_Client_writeValueAttribute(client, UA_NODEID_STRING(ns, (char*) node_id_xml.c_str()), &val);
	pthread_mutex_unlock(&mutex);
	if (retval != UA_STATUSCODE_GOOD) {
		recGblSetSevr(prec, WRITE_ALARM, INVALID_ALARM);

		return -1;
	}

	return 0;
}


/* longout */

static long init_longout_record(struct longoutRecord *prec);
static long write_longout(struct longoutRecord *prec);
struct longout_DevSet_type {
	long number;
	DEVSUPFUN report;
	DEVSUPFUN init;
	long (*init_longout_record)(struct longoutRecord*);
	DEVSUPFUN get_ioint_info;
	long (*write_longout)(struct longoutRecord*);
	DEVSUPFUN special_linconv;
} longout_DevSet = {
	6,
	NULL,
	NULL,
	init_longout_record,
	NULL,
	write_longout,
	NULL
};
epicsExportAddress(dset, longout_DevSet);

static long init_longout_record(struct longoutRecord *prec) {
	if (prec->out.type != INST_IO) {
		recGblRecordError(S_db_badField, (void*) prec, "longout_DevSet (init_longout_record) Illegal OUT field");

		return S_db_badField;
	}

	return 2;
}

/*
0: Success.
Other: Error.
*/
static long write_longout(struct longoutRecord *prec) {
	int ns;
	std::string node_id_xml;
	UA_Variant val;
	UA_StatusCode retval;


	if (prec->out.type != INST_IO) {
		recGblRecordError(S_db_badField, (void*) prec, "longout_DevSet (write_longout) Illegal OUT field");

		return S_db_badField;
	}

	ns = 4;
	node_id_xml = std::string(prec->out.value.instio.string);

	UA_Variant_init(&val);
	retval = UA_Variant_setScalarCopy(&val, &(prec->val), &UA_TYPES[UA_TYPES_INT16]);
	if (retval != UA_STATUSCODE_GOOD) {
		recGblSetSevr(prec, WRITE_ALARM, INVALID_ALARM);

		return -1;
	}

	pthread_mutex_lock(&mutex);
	retval = UA_Client_writeValueAttribute(client, UA_NODEID_STRING(ns, (char*) node_id_xml.c_str()), &val);
	pthread_mutex_unlock(&mutex);
	if (retval != UA_STATUSCODE_GOOD) {
		recGblSetSevr(prec, WRITE_ALARM, INVALID_ALARM);

		return -1;
	}

	return 0;
}


/* ao */

static long init_ao_record(struct aoRecord *prec);
static long write_ao(struct aoRecord *prec);
struct ao_DevSet_type {
	long number;
	DEVSUPFUN report;
	DEVSUPFUN init;
	long (*init_ao_record)(struct aoRecord*);
	DEVSUPFUN get_ioint_info;
	long (*write_ao)(struct aoRecord*);
	DEVSUPFUN special_linconv;
} ao_DevSet = {
	6,
	NULL,
	NULL,
	init_ao_record,
	NULL,
	write_ao,
	NULL
};
epicsExportAddress(dset, ao_DevSet);

static long init_ao_record(struct aoRecord *prec) {
	if (prec->out.type != INST_IO) {
		recGblRecordError(S_db_badField, (void*) prec, "ao_DevSet (init_ao_record) Illegal OUT field");

		return S_db_badField;
	}

	return 2;
}

/*
0: Success.
other: Error.
*/
static long write_ao(struct aoRecord *prec) {
	int ns;
	std::string node_id_xml;
	UA_Variant val;
	UA_StatusCode retval;


	if (prec->out.type != INST_IO) {
		recGblRecordError(S_db_badField, (void*) prec, "ao_DevSet (write_ao) Illegal OUT field");

		return S_db_badField;
	}

	ns = 4;
	node_id_xml = std::string(prec->out.value.instio.string);

	UA_Variant_init(&val);
	retval = UA_Variant_setScalarCopy(&val, &(prec->val), &UA_TYPES[UA_TYPES_DOUBLE]);
	if (retval != UA_STATUSCODE_GOOD) {
		recGblSetSevr(prec, WRITE_ALARM, INVALID_ALARM);

		return -1;
	}

	pthread_mutex_lock(&mutex);
	retval = UA_Client_writeValueAttribute(client, UA_NODEID_STRING(ns, (char*) node_id_xml.c_str()), &val);
	pthread_mutex_unlock(&mutex);
	if (retval != UA_STATUSCODE_GOOD) {
		recGblSetSevr(prec, WRITE_ALARM, INVALID_ALARM);

		return -1;
	}

	return 0;
}


/* stringout */

static long init_stringout_record(struct stringoutRecord *prec);
static long write_stringout(struct stringoutRecord *prec);
struct stringout_DevSet_type {
	long number;
	DEVSUPFUN report;
	DEVSUPFUN init;
	long (*init_stringout_record)(struct stringoutRecord*);
	DEVSUPFUN get_ioint_info;
	long (*write_stringout)(struct stringoutRecord*);
	DEVSUPFUN special_linconv;
} stringout_DevSet = {
	6,
	NULL,
	NULL,
	init_stringout_record,
	NULL,
	write_stringout,
	NULL
};
epicsExportAddress(dset, stringout_DevSet);

static long init_stringout_record(struct stringoutRecord *prec) {
	if (prec->out.type != INST_IO) {
		recGblRecordError(S_db_badField, (void*) prec, "stringout_DevSet (init_stringout_record) Illegal OUT field");

		return S_db_badField;
	}

	return 2;
}

/*
0: Success.
Other: Error.
*/
static long write_stringout(struct stringoutRecord *prec) {
	int ns;
	std::string node_id_xml;

	UA_String s;
	UA_Variant val;
	UA_StatusCode retval;


	if (prec->out.type != INST_IO) {
		recGblRecordError(S_db_badField, (void*) prec, "stringout_DevSet (write_stringout) Illegal OUT field");

		return S_db_badField;
	}

	ns = 4;
	node_id_xml = std::string(prec->out.value.instio.string);

	s = UA_String_fromChars(prec->val);
	UA_Variant_init(&val);
	retval = UA_Variant_setScalarCopy(&val, &s, &UA_TYPES[UA_TYPES_STRING]);
	if (retval != UA_STATUSCODE_GOOD) {
		recGblSetSevr(prec, WRITE_ALARM, INVALID_ALARM);

		return -1;
	}

	pthread_mutex_lock(&mutex);
	retval = UA_Client_writeValueAttribute(client, UA_NODEID_STRING(ns, (char*) node_id_xml.c_str()), &val);
	pthread_mutex_unlock(&mutex);
	if (retval != UA_STATUSCODE_GOOD) {
		recGblSetSevr(prec, WRITE_ALARM, INVALID_ALARM);

		return -1;
	}

	return 0;
}


int get_value(std::string node_id_xml, UA_Variant* p_value) {
	std::map<std::string, int>::iterator it;
	int index;
	UA_StatusCode status;


	if (!read_response.results) {
		return -1;
	}

	it = node_id_xml_to_index_map.find(node_id_xml);
	if (it == node_id_xml_to_index_map.end()) {
		return -1;
	}
	index = it->second;

	UA_StatusCode_init(&status);
	status = read_response.results[index].status;
	if (status != UA_STATUSCODE_GOOD) {
		return -1;
	}

	*p_value = read_response.results[index].value;

	return 0;
}


void* read_loop(void* arg) {
	UA_ClientConfig config;

	UA_StatusCode retval;

	UA_ReadRequest read_request;
	size_t node_id_xml_set_size;
	std::set<std::string>::iterator it;
	int i;
	std::string node_id_xml;
	int ns;

	UA_ClientState client_state;


	/* Populates the node_id_xml to index map. */
	for (it = node_id_xml_set.begin(), i = 0; it != node_id_xml_set.end(); ++it, ++i) {
		node_id_xml = *it;
		node_id_xml_to_index_map.insert( std::pair<std::string, int>(node_id_xml, i) );
	}


	/* Populates the read request structure. */

	UA_ReadRequest_init(&read_request);

	node_id_xml_set_size = (size_t) node_id_xml_set.size();
	read_request.nodesToReadSize = node_id_xml_set_size;
	read_request.nodesToRead = (UA_ReadValueId*) UA_Array_new(node_id_xml_set_size, &UA_TYPES[UA_TYPES_READVALUEID]);

	ns = 4;
	for (it = node_id_xml_set.begin(), i = 0; it != node_id_xml_set.end(); ++it, ++i) {
		node_id_xml = *it;

		read_request.nodesToRead[i].nodeId = UA_NODEID_STRING(ns, (char*) node_id_xml.c_str());
		read_request.nodesToRead[i].attributeId = UA_ATTRIBUTEID_VALUE;
	}


	UA_ReadResponse_init(&read_response);


	/* Creates the connection. */

	config = UA_ClientConfig_standard;
	config.timeout = 1000;

	client = UA_Client_new(config);

	printf("Connecting to %s.\n", url.c_str());
	retval = UA_Client_connect(client, url.c_str());
	if (retval == UA_STATUSCODE_GOOD) {
		printf("Connected.\n");
	}


	/* Periodically reads all of the values into the read_response structure. */
	while (1) {
		client_state = UA_Client_getState(client);
		if (client_state != UA_CLIENTSTATE_CONNECTED) {
			printf("Connection failed.\n");

			while (1) {
				printf("Connecting to %s.\n", url.c_str());
				retval = UA_Client_connect(client, url.c_str());
				if (retval == UA_STATUSCODE_GOOD) {
					break;
				}

				sleep(1);
			}

			printf("Connected.\n");
		}

		pthread_mutex_lock(&mutex);
		read_response = UA_Client_Service_read(client, read_request);
		pthread_mutex_unlock(&mutex);

		usleep(1);
	}
}


// URL
static void devOPCConfigureCallFunc(const iocshArgBuf *args) {
	printf("URL: %s\n", args[0].sval);

	url = std::string(args[0].sval);
}

static const iocshArg Arg0 = { "URL", iocshArgString };
static const iocshArg *const Arg_1[1] = { &Arg0 };
static const iocshFuncDef devOPCConfigureFuncDef = { "devOPC_URL", 1, Arg_1 };


static void devOPCRegistrar(void) {
	iocshRegister(&devOPCConfigureFuncDef, devOPCConfigureCallFunc);
}
epicsExportRegistrar(devOPCRegistrar);


static void trace(initHookState state) {
	int error;


	if (state == initHookAfterInitDatabase) {
		printf("initHookAfterInitDatabase\n");

		/* Creates the thread for periodically reading the values. */
		error = pthread_create(&thread, NULL, read_loop, NULL);
		if (error) {
			printf("Error: pthread_create: %i\n", error);
			exit(EXIT_FAILURE);
		}
	}
}

int traceIocInit(void) {
	static int done = 0;


	if (done) {
		return -1;
	}

	done = 1;

	initHookRegister(trace);

	return 0;
}


static const iocshFuncDef traceInitFuncDef = {"traceIocInit", 0, NULL};
static void traceInitFunc(const iocshArgBuf *args) {
	traceIocInit();
}

static void initTraceRegister(void) {
	iocshRegister(&traceInitFuncDef, traceInitFunc);
}
epicsExportRegistrar(initTraceRegister);

