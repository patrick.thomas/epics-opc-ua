#!../../bin/linux-x86_64/opc

## You may have to change opc to something else
## everywhere it appears in this file

#< envPaths

## Register all support components
dbLoadDatabase("../../dbd/opc.dbd",0,0)
opc_registerRecordDeviceDriver(pdbbase) 

## Load record instances
#dbLoadRecords("../../db/opc.db","user=patrick.thomas")

dbLoadRecords("../../db/tc3.db")

devOPC_URL "opc.tcp://10.11.0.67:4840"

traceIocInit

iocInit()

## Start any sequence programs
#seq sncopc,"user=patrick.thomas"
